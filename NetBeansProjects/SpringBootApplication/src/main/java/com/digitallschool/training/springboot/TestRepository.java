/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.springboot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author admin
 */
@Repository
public class TestRepository {

    @Autowired
    DataSource ds;

    public List<Items> getAllItems() {
        List<Items> ls = new ArrayList();

        try (Connection con = ds.getConnection();
                Statement ps = con.createStatement();
                ResultSet rs = ps.executeQuery("select* from items");) {

            while (rs.next()) {
                System.out.println("data is"+rs.getInt(1));
                ls.add(new Items(rs.getInt(1),rs.getString(2)));
            }

            return ls;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return ls;
    }

}
