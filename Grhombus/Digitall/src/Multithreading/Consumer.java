package Multithreading;

import java.util.Optional;



public class Consumer extends Thread {

    private Warehouse cw;
    private Optional<Integer> count;

    public Consumer(Warehouse cw) {
        this.cw = cw;
        this.count = Optional.empty();
    }
    public Consumer(Warehouse cw, Integer count){
        this.cw = cw;
        this.count = Optional.of(count);
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();

        for (int i = 0; i < count.orElse(10); i++) {
            System.out.println(name + ": " + cw.getGoods());

            try {
                Thread.sleep(200);
            } catch (InterruptedException ie) {

            }
        }
    }
}
