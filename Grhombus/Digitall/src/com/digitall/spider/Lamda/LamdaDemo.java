package com.digitall.spider.Lamda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.function.Function;

import javax.net.ssl.HttpsURLConnection;

public class LamdaDemo {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		HttpURLConnection con = (HttpURLConnection) URI
                //.create("http://192.168.6.55:8080/spring-web-spiders/members")
				.create("http://localhost:8081/SpringMVCDemo/authorAPI/1/name")
                .toURL().
                openConnection();
		BufferedReader in=new BufferedReader(new InputStreamReader(con.getInputStream()));
		
       String temp="";
        String result = "";
        
        //byte[] data = new byte[1024];
        while ((temp=in.readLine())!=null) {
            result += temp;
        }
        System.out.println(result);
        con.disconnect();
		
                /*int a=5;
		
		Function<Integer,Double> i=(e)->Math.pow(e,3);
		System.out.println(i.apply(4));*/
	}

}
