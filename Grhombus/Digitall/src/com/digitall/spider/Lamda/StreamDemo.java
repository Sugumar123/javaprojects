package com.digitall.spider.Lamda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamDemo {
	
	public static void main(String[] args)
	{
		List<Integer> value1=Arrays.asList(10,20,30);
		Integer[] value2= {5,10,15,16};
		
		List<Integer> values=(List<Integer>) Stream.concat(value1.stream(),Arrays.stream(value2))
				             .filter(e->e%4==0).collect(Collectors.toList());
		
		System.out.println(values);
		
		
		
	}
	

	public static void main3(String[] args) {
		// TODO Auto-generated method stub
		
		
		List <List<Integer>> a=new ArrayList();
		
		a.add(Arrays.asList(10,20,30));
		a.add(Arrays.asList(15,25,35));
		
		List<Integer> mod=a.stream().flatMap(e->e.stream()).filter(e->e%3==0).collect(Collectors.toList());

	}
	
	public static void main2(String[] args)
	{
        List<String> a=Arrays.asList("James","Dennis");
		
		int length=a.stream().mapToInt(e->e.length()).sum();
		
		System.out.println(length);
	}

}
