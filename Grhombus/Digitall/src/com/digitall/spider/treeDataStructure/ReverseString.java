package com.digitall.spider.treeDataStructure;

public class ReverseString {
	
	public static StringBuffer reverse(String s)
	{
		StringBuffer exp = new StringBuffer(s);
		return exp.reverse();
	}

}
