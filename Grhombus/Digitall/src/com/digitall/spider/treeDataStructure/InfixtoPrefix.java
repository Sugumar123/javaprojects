package com.digitall.spider.treeDataStructure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InfixtoPrefix {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int n = 0, m = 0;
		int top = 0;

		String exp = br.readLine();
		exp = new String(ReverseString.reverse(exp));
		char[] prefix = new char[exp.length()];
		Character[] stack = new Character[exp.length()];

		System.out.println(stack[0]);

		for (int i = 0; i < exp.length(); i++) {
			if (Character.isLetter(exp.charAt(i))) {
				prefix[n] = exp.charAt(i);
				n++;
			} else if (exp.charAt(i) == '(') {

				for (; top >= 0; top--) {
					if (stack[top] == ')') {
						stack[top] = null;
						top--;
						break;
					} else {
						prefix[n] = stack[top];
						++n;
						m--;

					}
				}

			}

			else if (exp.charAt(i) == '+' || exp.charAt(i) == '-' || exp.charAt(i) == '*' || exp.charAt(i) == '/'
					|| exp.charAt(i) == '%' || exp.charAt(i) == '^' || exp.charAt(i) == '&' || exp.charAt(i) == ')') {

				if (exp.charAt(i) == '*' || exp.charAt(i) == '/' || exp.charAt(i) == ')') {

					stack[m] = exp.charAt(i);
					top = m;
					m++;

				} else if (exp.charAt(i) == '+' || exp.charAt(i) == '-') {
					if (top < 0)
						top = 0;
					if (stack[top] == null || stack[top] == '+' || stack[top] == '-' || stack[top] == ')'
							|| stack[top] == '(') {
						stack[m] = exp.charAt(i);
						top = m;
						m++;
					}

					else if (stack[top] == '*' || stack[top] == '/') {
						int s = 0;
						while (top >= 0 && m >= 0 && (stack[top] == '*' || stack[top] == '/')) {
							if (s == 0) {
								m = m - 1;
								s = s + 1;
							}
							prefix[n] = stack[top];
							++n;
							m--;
							top = m;
						}
						m = m + 1;
						stack[m] = exp.charAt(i);
						top = m;
						m++;
					}
				}
			}

		}

		for (; top >= 0; top--) {
			if (stack[top] != null) {
				prefix[n] = stack[top];
				++n;
			}
		}

		StringBuffer prefix1 = new StringBuffer(new String(prefix));

		System.out.println("\nPrefix is:" + " " + prefix1.reverse());

		/*
		 * for(int i=0;i<stack.length;i++) { System.out.println(stack[i]); }
		 */

	}

}
