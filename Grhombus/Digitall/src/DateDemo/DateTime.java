package DateDemo;

import java.time.Month;
import java.time.MonthDay;
import java.time.YearMonth;



public class DateTime {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		YearMonth a1=YearMonth.now();
		System.out.println(a1);
		YearMonth a2=a1.withYear(2025);
		System.out.println(a2);
		YearMonth a3=a1.withMonth(3);
		System.out.println(a3);
		
		MonthDay m1=MonthDay.now();
		//System.out.println(m1);
		MonthDay m2=m1.withMonth(3);
		//System.out.println(m2);
		MonthDay m3=m1.withDayOfMonth(3);
		
		MonthDay m4=m1.with(Month.AUGUST);
		
		
        
		System.out.println(m1+" "+m2+" "+m3+" "+m4);
		
	}

}
