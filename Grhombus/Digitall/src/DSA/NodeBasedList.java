package DSA;

public class NodeBasedList {
	private Node head;
	private int size;
	
	{
		
		head=null;
		size=0;
	}
	
	private static class Node{
		
		
		private Node previous;
		private Node next;
		Object data;
		
		Node(Node previous, Node next,Object data)
		{
			this.previous=previous;
			this.next=next;
			this.data=data;
		}
	}
	// Checking Empty or Not Method
	public boolean isEmpty()
	{
		return (head==null)?true: false;
		
	}
	// Add Method as an argument as element
	public void add(Object e)
	{
		Node temp=new Node(null,null,e);
		Node tem=head;
		if(isEmpty())
		{
			head=temp;
			size++;
		}
		else
		{
			while(tem.next!=null)
			{
				tem=tem.next;
			}
			tem.next=temp;
			temp.previous=tem;
			size++;
		}
		
	}
	//Add a element at particular index
	public void add(int index,Object e)
	{
		if(index==0)
		{
			addHead(e);
		}
		else
		{
			addPosition(index,e);
			
		}
	}
	//Add element at Head
	public void addPosition(int index,Object e)
	{
		Node temp=new Node(null,null,e);
		Node tem=head;
		if(isEmpty())
		{
			head=temp;
			size++;
		}
		else
		{
			for(int i=1;i<=index;i++)
			{
				tem=tem.next;
			}
			tem.previous.next=temp;
			temp.previous=tem.previous;
			temp.next=tem;
			tem.previous=temp;
			size++;
		}
	}
	public void addAll(NodeBasedList a2)
	{
		Node temp=a2.head;
		
		for(int i=0;i<a2.size;i++)
		{
			this.add(temp.data);
			temp=temp.next;
		}
	}
	public void addHead(Object e)
	{
		Node temp=new Node(null,null,e);
		Node tem=head;
		temp.next=head;
		//head=null;
		/*for(int i=0;i<size;i++)
			{tem=tem.next;}*/
		head=temp;
		size++;
		
	}
	
	
	//Remove Method
	public void remove(Object e)
	{
				
		Node tem=head;int flag=0;
		Node prev=null; int i=0;
		
		while(tem.next!=null)
		{
			if((tem.data).equals(e))
			{
				flag=1;
				i++;
				break;
			}
			i++;
			
			prev=tem;
			tem=tem.next;
		}
		if(i==1)
		{
			prev=null;
			removeHead();
		}
		if(i!=1){
		prev.next=tem.next;
		tem.previous=prev;
		tem=null;
		size--;
		}
		
		
	}
	//Remove a element using index
	public void remove(int index)
	{
		if(size==0||index>size-1)
			{System.out.println("Not able to remove");}
		Node temp=head;
		if(index==0)
		removeHead();
		else{
		for(int i=1;i<=index;i++)
		{
			temp=temp.next;
		}
		temp.next.previous=temp.previous;
		temp.previous.next=temp.next; 
		
		//temp=null;
		size--;
		}
		
		
	}
	 void removeHead()
	{
		Node tem=head;
		head=null;
		head=tem.next;
		head.previous=null;
		size--;
	}
	 //Remove All a2 elements in a1
	 
	 public void removeAll(NodeBasedList a2)
	 {
		 Node temp=a2.head;
		 for(int i=0;i<a2.size;i++)
		 {
			 this.remove(temp.data);
			 temp=temp.next;
		 }
	 }
	 
	/* private Object removeNode(Node current)
	 {
		 current.next.previous=current.previous;
		 return current.data;
	 }*/
	 public boolean contains(Object e)
	 {
		 Node temp=head;boolean flag=false;
		 for(int i=0;i<this.size;i++)
		 {
			 if(temp.data.equals(e))
				 {flag=true;}
			temp=temp.next; 
				 
		 }
		 return flag;
	 }
	 //ContainsAll method
	 public boolean containsAll(NodeBasedList a2)
	 {
		 Node temp=a2.head; boolean flag=true;
		 
		 for(int i=0;i<a2.size;i++)
		 {
			 
			 if(!this.contains(temp.data))
			 {
				 flag=false;
				 break;
				 }
			 temp=temp.next;
		 }
		 return flag;
		 
	 }
	 //RetainAll
	 public void retainsAll(NodeBasedList a2,NodeBasedList a3)
	 {
		 Node temp=a2.head;
		 for(int i=0;i<a2.size;i++)
		 {
			 if(this.contains(temp.data))
			 {
				 a3.add(temp.data);
			 }
			 temp=temp.next;
		 }
		 
		 System.out.println(a3.toString());
		
	 }
	 //Return the element from particular index
	 public Object indexof(int index)
	 {
		 Node temp=head;
		 if(index>=size)
		 {
			 System.out.println("Index is not available");
			 return null;
		 }
		 else{
		 for(int i=0;i<index;i++)
		 {
			 temp=temp.next;
		 }}
		 return temp.data;
	 }
	 
	public String toString()
	{
		StringBuilder builder=new StringBuilder();
		builder.append("[");
		Node temp=head;int i=0;
		for(;i<size;i++)
		{
			builder.append(temp.data+",");
			temp=temp.next;
		}
		builder.append("]");
		
		return builder.toString();
	}
	

	

}
