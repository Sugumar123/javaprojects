package com.digitallschool.refactor;

public interface Shipping {

	
	public void processShipping();
	
	public boolean selectShipping();
}
