package com.digitallschool.refactor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CustomerCr extends Order {
	

	
	private int customerId;
     private String customerName;

    private String customerAddress1;
    private String customerAddress2;
    private String customerAddress3;
    private String currentAddress;

    private String mobileNumber1;
    private String mobileNumber2;
    private String mobileNumber3;
    
    public CustomerCr(int customerId,String customerName,String customerAddress1,String mobileNumber1) {
		setCustomerId(customerId);
		setCustomerName(customerName);
		setCustomerAddress1(customerAddress1);
		setMobileNumber1(mobileNumber1);
    }
    
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress1() {
		return customerAddress1;
	}
	public void setCustomerAddress1(String customerAddress1) {
		this.customerAddress1 = customerAddress1;
	}
	public String getCustomerAddress2() {
		return customerAddress2;
	}
	public void setCustomerAddress2(String customerAddress2) {
		this.customerAddress2 = customerAddress2;
	}
	public String getCustomerAddress3() {
		return customerAddress3;
	}
	public void setCustomerAddress3(String customerAddress3) {
		this.customerAddress3 = customerAddress3;
	}
	public String getCurrentAddress() {
		return currentAddress;
	}
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}
	public String getMobileNumber1() {
		return mobileNumber1;
	}
	public void setMobileNumber1(String mobileNumber1) {
		this.mobileNumber1 = mobileNumber1;
	}
	public String getMobileNumber2() {
		return mobileNumber2;
	}
	public void setMobileNumber2(String mobileNumber2) {
		this.mobileNumber2 = mobileNumber2;
	}
	public String getMobileNumber3() {
		return mobileNumber3;
	}
	public void setMobileNumber3(String mobileNumber3) {
		this.mobileNumber3 = mobileNumber3;
	}











	
}
