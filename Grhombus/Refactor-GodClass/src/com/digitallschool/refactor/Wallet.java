/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.refactor;

import java.time.YearMonth;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Wallet {

    private int fiftyNPCoins;
    private int oneRupeeCoins;
    private int twoRupeeCoins;
    private int fiveRupeeCoins;
    private int tenRupeeCoins;

    private int fiveRupeeNotes;
    private int tenRupeeNotes;
    private int twentyRupeeNotes;
    private int fiftyRupeeNotes;
    private int hundredRupeeNotes;
    private int fiveHundredRupeeNotes;
    private int thousandRupeeNotes;
    private int twoThousandRupeeNotes;

    private long[] debitCardNumbers;
    private YearMonth[] debitCardIssueDates;
    private YearMonth[] debitCardExpiryDates;
    private String[] nameOnDebitCards;
    private short[] debitCardCVVNumbers;

    private long[] creditCardNumbers;
    private YearMonth[] creditCardIssueDates;
    private YearMonth[] creditCardExpiryDates;
    private String[] nameOnCreditCards;
    private short[] creditCardCVVNumbers;
    private double[] creditCardCreditLimits;
    private double[] creditCardAvailableLimits;

    public Wallet() {
        super();
    }

    public YearMonth getExpiryDateOfCreditCard(long cardNumber) {
        return null;
    }

    public YearMonth getIssuedDateOfCreditCard(long cardNumber) {
        return null;
    }

    public String getNameOnCreditCard(long cardNumber) {
        return "";
    }

    public double getCreditCardAvailableLimit(long cardNumber) {
        return 0;
    }

    public double getCreditCardCreditLimit(long cardNumber) {
        return 0;
    }

    public double getCreditCardAvailableLimit() {
        return 0;
    }

    public double getCreditCardCreditLimit() {
        return 0;
    }

    public double getNoteAmount() {
        return 0;
    }

    public double getCoinAmount() {
        return 0;
    }

    public double getAmount() {
        return 0;
    }

    public void addFiveNotes(int notes) {

    }

    public boolean removeFiveNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addTenNotes(int notes) {

    }

    public boolean removeTenNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addTwentyNotes(int notes) {

    }

    public boolean removeTwentyNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addFiftyNotes(int notes) {

    }

    public boolean removeFiftyNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addHundredNotes(int notes) {

    }

    public boolean removeHundredNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addTwoHundredNotes(int notes) {

    }

    public boolean removeTwoHundredNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addFiveHundredNotes(int notes) {

    }

    public boolean removeFiveHundredNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addThousandNotes(int notes) {

    }

    public boolean removeThousandNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addTwoThousandNotes(int notes) {

    }

    public boolean removeTwoThousandNotes(int notes) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addFiftyNPCoins(int coins) {

    }

    public boolean removeFiftyNPCoins(int coins) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addOneRupeeCoins(int coins) {

    }

    public boolean removeOneRupeeCoins(int coins) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addTwoRupeeCoins(int coins) {

    }

    public boolean removeTwoRupeeCoins(int coins) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addFiveRupeeCoins(int coins) {

    }

    public boolean removeFiveRupeeCoins(int coins) {
        throw new UnsupportedOperationException("To be finished");
    }

    public void addTenRupeeCoins(int coins) {

    }

    public boolean removeTenRupeeCoins(int coins) {
        throw new UnsupportedOperationException("To be finished");
    }

    //YOU WILL GET MANY MUTATIONS OF COINS and NOTES
    public void addOneAndTwoRupeeCoins(int oneRupeeCoins, int twoRupeeCoins) {

    }

    public boolean removeOneAndTwoRupeeCoins(int oneRupeeCoins, int twoRupeeCoins) {
        throw new UnsupportedOperationException("To be finished");
    }
}
