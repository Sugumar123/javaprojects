package com.digitallschool.spider.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAMain {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

EntityManagerFactory emf=Persistence.createEntityManagerFactory("learn-hibernate");
		
		EntityManager em=emf.createEntityManager();
		
		AuthorOperation a=new AuthorOperation();
		a.authorOper(em);
		
		/*List<Author> obj=em.createQuery("SELECT a from Author a", Author.class).getResultList();
	       
		for(Author e:obj)
        	System.out.println(e.getId()+" "+e.getAuthor_name()+" "+e.getEmail());*/
		
		
		
		
		 em.close();
	        emf.close();
	}

}
