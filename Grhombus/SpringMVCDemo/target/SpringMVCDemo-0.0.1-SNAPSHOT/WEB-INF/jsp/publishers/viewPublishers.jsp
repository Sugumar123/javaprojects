<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
<link href="<c:url value="/resources/theme1/css/main.css" />" rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
 body{
 background-color: aqua;
}
</style>
</head>
<body>
<form action="add" method="post">
			<table style="width: 50%" align="left">
			
			<tr>
					<td>Publisher_ID:</td>
					<td><input type="text" name="publisherId" /></td>
				</tr>
				<tr>
					<td>Publisher_Name:</td>
					<td><input type="text" name="name" /></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><input type="text" name="email" /></td>
				</tr>
				<tr>
					<td>Mobile:</td>
					<td><input type="text" name="mobile" /></td>
				</tr>
				<tr>
					<td>Address:</td>
					<td><input type="text" name="address" /></td>
				</tr>
				
					
			<tr><td><input type="submit" value="Submit" /></td></tr></table></form>
<c:choose>
		<c:when test="${not empty publishersData}">
	
			<table border="1" align="right">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Email</th>
				</tr>
				<c:forEach items="${publishersData}" var="pub">
					<tr>
						<td>${pub.publisherId}</td>
						<td>${pub.name}</td>
						<td>${pub.email}</td>
					</tr>
					</c:forEach>
			</table>
		</c:when>
		<c:otherwise>
		<h3>NO PUBLISHERS</h3>
		</c:otherwise>
</c:choose>
		<br>
		<br>
</body>
</html>