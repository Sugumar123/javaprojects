package com.digitallschool.spring_mvc_demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {
	
	@RequestMapping("/hi")
	public String sayHello()
	{
		return "hello2";
	}

	
}
