package com.digitallschool.spring_mvc_demo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

//import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

//import com.mysql.cj.protocol.Resultset;

@Repository
public class PublisherRepository {

	@Autowired
	DataSource dataSource;

	public List<Publisher> getPublisher() throws SQLException {
		List<Publisher> pl = new ArrayList<>();

		try (Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement("select* from publishers");) {
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				pl.add(new Publisher(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
			}
		}

		return pl;

	}
	
	public void addPublisher(Publisher p1) throws SQLException{
		
		try (Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement("insert into publishers(publisher_id,name,email,contact,address)"+"values(?,?,?,?,?);");){
			
			ps.setInt(1, p1.getPublisherId());
			ps.setString(2, p1.getName());
			ps.setString(3, p1.getEmail());
			ps.setString(4, p1.getMobile());
			ps.setString(5, p1.getAddress());
			
			ps.executeUpdate();
			
			System.out.println("Publisher is Inserted");
			
			
		}
		
		
	}
}
