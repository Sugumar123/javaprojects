package com.digitallschool.spring_mvc_demo;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherService {
	
	@Autowired
	PublisherRepository publisherRepository;
	
	public List<Publisher> getPublishers() throws SQLException
	{
		return publisherRepository.getPublisher();
	}
	
	public void addPublishers(Publisher p1) throws SQLException{
		
		
		publisherRepository.addPublisher(p1);
	}

}
