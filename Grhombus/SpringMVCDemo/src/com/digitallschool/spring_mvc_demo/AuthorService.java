package com.digitallschool.spring_mvc_demo;



import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AuthorService {

    @Autowired
    AuthorRepository ar;

    public boolean addAuthor(Author author) {
        if (Objects.isNull(author) || Objects.isNull(author.getName().trim())
                || author.getName().trim().length() == 0) {
            return false;
        } else {
            return ar.addAuthor(author);
        }
    }

    public List<Author> getAllAuthors() {
        return ar.getAuthors();
    }

    public Author getAuthorById(int authorId) {
        return ar.getAuthor(authorId);
    }
    
    public boolean updateAuthor(Author author) throws SQLException{
        return ar.updateAuthor(author);
    }
    public boolean deleteAuthor(int authorId)
    {
    	return ar.deleteAuthor(authorId);
    }
    
  /* public List<Author> getParticularAuthors()
    {
    	return ar.getParticularAuthors();
    }*/
}
