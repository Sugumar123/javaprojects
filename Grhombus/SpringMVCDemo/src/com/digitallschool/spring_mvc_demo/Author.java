package com.digitallschool.spring_mvc_demo;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Author {
	
private int authorId;
    
    @NotNull(message = "Name cannot be empty")
    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 5, message = "Name at least {min} characters length")
    private String name;
    
    @Email(message = "Email format wrong")
    private String email;
    private String profile;
    private String blog;

    public Author() {
        super();
    }

    public Author(int authorId, String name) {
        this.authorId = authorId;
        this.name = name;
    }

    public Author(int authorId, String name, String email, String profile, String blog) {
        this.authorId = authorId;
        this.name = name;
        this.email = email;
        this.profile = profile;
        this.blog = blog;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    @Override
    public String toString() {
        return "Author{" + authorId + ", " + name + ", " + email + ", "
                + profile + ", " + blog + "}";
    }


}
