package com.digitallschool.spring_mvc_demo;





public class Publisher {
	
	Publisher()
	{
		super();
	}
	
	private int publisherId;
	private String name;
	private String address;
	private String mobile;
	private String email;
	
	
	public Publisher(int publisherId,String name,String address,String mobile,String email)
	{
		this.publisherId=publisherId;
		this.name=name;
		this.address=address;
		this.mobile=mobile;
		this.email=email;
	}
	
	
	public int getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString()
	{
		return  "Publisher[" + publisherId + ", " +name + ", " + address + ", " + mobile + ", " + email + "]";}

}
