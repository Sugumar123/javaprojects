package com.digitallschool.spring_mvc_demo;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/publisher")
public class PublisherController {

	// @Autowired
	PublisherService publisherService;

	public PublisherController(PublisherService service) {
		this.publisherService = service;
	}

	@RequestMapping("/all")
	public String viewPublishers(Model model) throws SQLException {

		model.addAttribute("publishersData", publisherService.getPublishers());

		return "publishers/viewPublishers";
	}

	@RequestMapping(value = "/publishers/allDirect", produces = "application/json")
	@ResponseBody
	public List<Publisher> viewPublishers1(Model model) throws SQLException {

		return publisherService.getPublishers();
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addPublishers(/*@ModelAttribute("Publisher")*/ Publisher publisher) throws SQLException {

		Publisher p1 = new Publisher(publisher.getPublisherId(), publisher.getName(), publisher.getMobile(),
				publisher.getAddress(), publisher.getEmail());
		publisherService.addPublishers(p1);

		return "redirect:/publisher/all";

	}
}
