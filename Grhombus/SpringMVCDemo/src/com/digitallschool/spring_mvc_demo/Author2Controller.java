package com.digitallschool.spring_mvc_demo;

import java.sql.SQLException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/authors2")
public class Author2Controller {

    @Autowired
    AuthorService service;
    
  

    @GetMapping
    public String viewAuthors(Model m, @ModelAttribute("author") Author author) {
        m.addAttribute("allAuthors", service.getAllAuthors());
        return "authors/home";
    }

    @PostMapping
    public String addAuthor(@Valid @ModelAttribute("author") Author currentAuthor,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/authors/home";
        }
   
        service.addAuthor(currentAuthor);
        return "redirect:/authors2";
    }

    @GetMapping("/updateForm")
    //public String updateAuthorForm(@RequestParam("aid") int authorId, Model m) throws SQLException {
    public String updateAuthorForm(Model m,@RequestParam("aid") int authorId) throws SQLException {
        m.addAttribute("currentAuthor", service.getAuthorById(authorId));
        m.addAttribute("allAuthors", service.getAllAuthors());
        return "authors/updateAuthor";
    }

    @PostMapping("/update")
    public String updateAuthor(@ModelAttribute("author") Author currentAuthor/*@RequestParam("aid") int authorId*/) throws SQLException {
        service.updateAuthor(currentAuthor);
        return "redirect:/authors2";
    }

    @ModelAttribute("author")
    public Author setupAuthor() {
        return new Author();
    }
}
