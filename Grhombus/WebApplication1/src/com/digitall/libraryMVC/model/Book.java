package com.digitall.libraryMVC.model;

public class Book {
	
	
	private int bookId;
	private String title;
	private String edition;
	private int price;
	private int published_year;
	private int published_by;
	
	public Book(int bookId,String title,String edition,int price,int published_year,int published_by) {
		
		this.bookId=bookId;
		this.title=title;
		this.edition=edition;
		this.price=price;
		this.published_year=published_year;
		this.published_by=published_by;
	}
	
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getPublished_year() {
		return published_year;
	}
	public void setPublished_year(int published_year) {
		this.published_year = published_year;
	}
	public int getPublished_by() {
		return published_by;
	}
	public void setPublished_by(int published_by) {
		this.published_by = published_by;
	}
	
	

}
