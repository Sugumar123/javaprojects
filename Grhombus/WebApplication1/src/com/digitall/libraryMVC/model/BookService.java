package com.digitall.libraryMVC.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookService {
	
	private static String jdbc_driver = "com.mysql.jdbc.Driver";
    private String jdbc_url = "jdbc:mysql://localhost:3306/library";
    private String username = "root";
    private String pwd = "root";

    static {
        try {
            Class.forName(jdbc_driver);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }
    public List<Book> getBook() throws SQLException {
        List<Book> Book = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(jdbc_url, username, pwd);
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM books")) {

            while (rs.next()) {
                Book.add(new Book(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6)));
            }

        } catch (SQLException sqle) {
            throw sqle;
        }

        return Book;
    }

}
