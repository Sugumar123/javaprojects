package com.digitall.libraryMVC.model;

public class Publisher {
	
	private int publisher_id;
	private String name;
	private String email;
	private String contact;
	private String address;
	
	public Publisher(int publisher_id,String name,String email,String contact,String address)
	{
		this.publisher_id=publisher_id;
		this.name=name;
		this.email=email;
		this.contact=contact;
		this.address=address;
	}
	
	
	
	public int getPublisher_id() {
		return publisher_id;
	}
	public void setPublisher_id(int publisher_id) {
		this.publisher_id = publisher_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	

}
