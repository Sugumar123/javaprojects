package com.digitall.libraryMVC.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PublisherService {
	
	private static String jdbc_driver = "com.mysql.jdbc.Driver";
    private String jdbc_url = "jdbc:mysql://localhost:3306/library";
    private String username = "root";
    private String pwd = "root";

    static {
        try {
            Class.forName(jdbc_driver);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }
    public List<Publisher> getPublisher() throws SQLException {
        List<Publisher> Publisher = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(jdbc_url, username, pwd);
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM publishers")) {

            while (rs.next()) {
                Publisher.add(new Publisher(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)));
            }

        } catch (SQLException sqle) {
            throw sqle;
        }

        return Publisher;
    }

}
