package com.digitall.libraryMVC.model;

public class Author {
	
	private int authorId;
    private String name;
    private String email;
    private String profile;
    private String blog;

    public Author() {
        super();
    }

    public Author(int authorId, String name, String email, String profile, String blog) {
        this.authorId = authorId;
        this.name = name;
        this.email = email;
        this.profile = profile;
        this.blog = blog;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

}
