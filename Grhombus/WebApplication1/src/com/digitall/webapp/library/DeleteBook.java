package com.digitall.webapp.library;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.lang.*;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteBook
 */
@WebServlet("/DeleteBook")
public class DeleteBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	doPost(request,response);
    	/*PrintWriter out=response.getWriter();
    	response.setContentType("text/html");
    	out.println("<form action='DeleteBook' method='post'>"
    			+"<table>"
    			+ "<tr><input type='submit' value='Final Confirmation from you'/></tr>"
    			);*/
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		
    	
    try {
    	
    	Class.forName("com.mysql.cj.jdbc.Driver");

		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");){
			
			
			
			String sql="delete from books where book_id=(?)";
			String sql2="delete from book_authors where book_id=(?)";
			
			
			PreparedStatement ps1=con.prepareStatement(sql);
			PreparedStatement ps2=con.prepareStatement(sql2);
			
			ps1.setInt(1, Integer.parseInt(request.getParameter("bid")));
			ps2.setInt(1, Integer.parseInt(request.getParameter("bid")));
			
			ps2.executeUpdate();
			ps1.executeUpdate();
			//ps2.executeUpdate();
			
			out.println("<h1>Book is deleted</h1>");
			response.sendRedirect("Book");
			
		
			
		}
    	
    	
    	
    	
    
    
    catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	               }
      }
      catch(ClassNotFoundException e)
        {
          e.printStackTrace();
         }
    	
    	
    	
	}

}
