package com.digitall.webapp.library;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Year;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//@WebServlet(urlPatterns = {"/books"})
@WebServlet("/Book")
public class Book extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html><head><title>Authors Home</title></head><body>");

        RequestDispatcher rd=request.getRequestDispatcher("/MainNavs");
        rd.include(request, response);

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");
                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery("SELECT * FROM books");) {

                if (rs.next()) {
                    out.println("<table style='float:left' border='1'>"
                            + "<caption>Books Details</caption>"
                            + "<tr><th>Book ID</th><th>Title</th><th>Edition</th><th>Price</th><th>Year</th></tr>");
                    do {
                        out.println("<tr>"
                                + "<td>" + rs.getInt(1) + "</td>"
                                + "<td>" + rs.getString(2) + "</td>"
                                + "<td>" + rs.getInt(3) + "</td>"
                                + "<td>" + rs.getInt(4) + "</td>"
                                + "<td>" + rs.getInt(5) + "</td>"
                                + "<td>" + "<a href='DeleteBook?bid="+rs.getInt(1)+"'>Delete</a>"+"</td>"
                                + "</tr>");
                    } while (rs.next());
                    out.println("</table>");
                } else {
                    out.println("<h2>No Books available.</h2>");
                }
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace(out);
        }

        out.println("<form method='post' action='Book' style='float:right'>"
                + "<table><tr><th colspan='2'>New Book Details</th></tr>"
                + "<tr><td>Title</td><td><input type='text' name='title' size='45'/></td></tr>"
                + "<tr><td>Edition</td><td><input type='text' name='edition' size='4'/></td></tr>"
                + "<tr><td>Price</td><td><input type='text' name='price'/></td></tr>"
                + "<tr><td>Year of Publishing</td><td><input type='text' name='year' size='8'/></td></tr>");

        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT publisher_id, name FROM publishers");) {

            if (rs.next()) {
                out.println("<tr><td>Publishers</td><td><select name='pid'><option value=''>-- Select Publisher --</option>");
                do {
                    int publisherId = rs.getInt(1);
                    String name = rs.getString(2);
                    out.println("<option value='" + publisherId + "'>" + name + "</option>");
                } while (rs.next());
                out.println("</select></td></tr>");
            }
        } catch (Exception e) {
            e.printStackTrace(out);
        }

        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT author_id, name FROM authors");) {

            if (rs.next()) {
                out.println("<tr><td>Authors</td><td><select name='aid' multiple size='5'><option value=''>-- Select Authors --</option>");
                do {
                    int authorId = rs.getInt(1);
                    String name = rs.getString(2);
                    out.println("<option value='" + authorId + "'>" + name + "</option>");
                } while (rs.next());
                out.println("</select></td></tr>");
            }
        } catch (Exception e) {
            e.printStackTrace(out);
        }

        out.println("<tr><td colspan='2' align='right'><input type='submit' value='Add Book'/></td></tr>"
                + "</table></form>");

        out.println("</body></html>");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");
                    PreparedStatement pst = con.prepareStatement("INSERT INTO books VALUES(0, ?, ?, ?, ?, ?)");
                    PreparedStatement pst2 = con.prepareStatement("INSERT INTO book_authors VALUES(0, ?, ?, ?)")) {
                con.setAutoCommit(false);
                
                pst.setString(1, request.getParameter("title"));
                pst.setInt(2, Integer.parseInt(request.getParameter("edition")));
                pst.setInt(3, Integer.parseInt(request.getParameter("price")));
                //pst.setDate(4, Date.valueOf(request.getParameter("year") + "-01-01"));
                pst.setInt(4, Integer.parseInt(request.getParameter("year")));
                pst.setInt(5, Integer.parseInt(request.getParameter("pid")));

                pst.executeUpdate();
                
                int bookId = 0;
                PreparedStatement pst3 = 
                        con.prepareStatement("SELECT book_id FROM books WHERE title=? AND edition=? AND price=? AND published_by=?");
                pst3.setString(1, request.getParameter("title"));
                pst3.setInt(2, Integer.parseInt(request.getParameter("edition")));
                pst3.setInt(3, Integer.parseInt(request.getParameter("price")));
                pst3.setInt(4, Integer.parseInt(request.getParameter("pid")));
                
                ResultSet rs = pst3.executeQuery();
                if(rs.next()){
                    bookId = rs.getInt(1);
                }
                
                
                String[] authorIdList = request.getParameterValues("aid");
                pst2.setInt(1, bookId);
                pst2.setString(3, "");
                
                for(String id: authorIdList){
                    pst2.setInt(2, Integer.parseInt(id));
                    pst2.executeUpdate();
                }
                
                con.commit();
                con.setAutoCommit(true);
                response.sendRedirect("Book");
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace(out);
        }
    }
}
