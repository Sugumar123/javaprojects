package com.digitall.webapp.library;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Publisher
 */
@WebServlet("/Publisher")
public class Publisher extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Publisher() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out = response.getWriter();

		out.println("<html><head><meta charset=\"UTF-8\">\r\n"
				+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n"
				+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n"
				+ "    <title>Document</title>\r\n" + "    <link href=\"style.css\" rel=\"stylesheet\"/>\r\n"
				+ "    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\" \r\n"
				+ "    integrity=\"sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu\" crossorigin=\"anonymous\"/>"
				+ "<style>\r\n" + 
				"  .bg-1 {\r\n" + 
				"    background-color: #1abc9c; /* Green */\r\n" + 
				"    color: #ffffff;\r\n" + 
				"  }\r\n" + 
				"  </style></head><body>");
		
		request.getRequestDispatcher("/MainNavs").include(request, response);
		
		
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");
					Statement st = con.createStatement();
					ResultSet rs = st.executeQuery("select* from publishers");) {

				;
				out.print("<div class='row'>"+"<div class='col-lg-1'></div>"+"<div class='col-lg-6 '>");
				//out.println("");
				if (rs.next()) {
					out.println("<table border=1><tr><th>Publisher Id</th><th>Name</th><th>Email-Id</th></tr>");
					do {

						out.println("<tr>" + "<td>" + rs.getInt(1) + "</td>" + "<td>" + rs.getString(2) + "</td>"
								+ "<td>" + rs.getString(3) + "</td>"

						);

					} while (rs.next());

					out.println("</table");
					
				}

			}

			catch (Exception e) {
				e.printStackTrace();
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace(out);
		}

		

		out.println("</div>");
		
		
		out.println("</div>");
		out.print("<div class='col-lg-5 bg-1'>");
		
		out.println("<form action='Publisher' method='post'>"
				+"<table class='text-center''><tr><th colspan='2'>New authors</th></tr>"
				+ "<tr><td>Publisher-Name</td></tr><td><input type='text' name='name' size='30'/></td>"
				+ "<tr><td>Email-id</td></tr><td><input type='text' name='email' size='30'/></td>"
				+ "<tr><td>Contact</td></tr><td><input type='text' name='contact' size='30'/></td>"
				+"<tr><td>Address</td></tr><td><input type='text' name='address' size='30'/></td>"
				+"<tr><td colspan='2' align='right'><input type='submit' value='Add Publisher'/></tr></table></form>");
	
		
		
		
		
		out.println("</div>");
		out.println("</body></html>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter out=response.getWriter();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");) {
				
				
				String sql="INSERT INTO publishers(publisher_id,name,email,contact,address)"+"VALUES(?,?,?,?,?)";
				PreparedStatement pstmt = con.prepareStatement(sql);
				pstmt.setInt(1, 0);
				pstmt.setString(2, request.getParameter("name"));
				pstmt.setString(3, request.getParameter("email"));
				pstmt.setString(4, request.getParameter("contact"));
				pstmt.setString(5, request.getParameter("address"));
				
				pstmt.executeUpdate();
				
				
				out.println("<h1>Publisher Has been Inserted</h1>");
				
			} 
			
			
			
			catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		
		
	}

}
