package com.digitall.webapp.library;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainNavigationServlet
 */
@WebServlet("/MainNavs")
public class MainNavigationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainNavigationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
    	PrintWriter out=response.getWriter();
    	out.print("<div class='row'>");
    	out.println("<div class='col-sm-2'>"+"</div>");
    	out.print("<div class='col-sm-3'>"+"<a href='PublisherServlet'>Publishers</a>"+"</div>");
    	out.print("<div class='col-sm-3'>"+"<a href='AuthorServlet'>Authors</a>"+"</div>");
    	out.print("<div class='col-sm-4'>"+"<a href='BookServlet'>Book</a>"+"</div>");
    	out.println("<br><hr color='darkgreen'/></br>");
    	//out.println("<a href='Publishers'>Publishers</a>"+"\t"+"<a href='Authors'>Authors</a>"+"\t"+"<a href='Book'>Book</a><br><hr color='darkgreen'/></br>");
    	
    	out.println("</div>");
        //out.println("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
