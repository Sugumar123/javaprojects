<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
     <%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
  <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="/MainNavs" /><br/>
<c:choose>
            <c:when test="${(empty requestScope.PublisherData)}">
                <h2>No Publisher Exist</h2>
            </c:when>
            <c:otherwise>
                <table border='1'>
                    <tr><th>ID</th><th>Name</th><th>Email</th><th>Ops</th></tr>
                            <c:forEach items="${requestScope.PublisherData}" var="publisher">
                        <tr>
                            <td>${publisher.publisher_id}</td>
                            <td>${publisher.name}</td>
                            <td>${publisher.email}</td>
                            <td>
                                <c:url var="deleteAuthorURL" value="authors">
                                    <c:param name="aid" value="${publisher.publisher_id}"/>
                                </c:url>

                                <a href='<c:out value="${deleteAuthorURL}"/>'>Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
        <br/><br/>

        

        
</body>
</html>