<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
 <%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
  <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <%-- <%@page contentType="text/html" pageEncoding="UTF-8"%> --%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.digitall.webapp.library.*" %>
<title>Insert title here</title>
</head>
<body>
      <%-- <jsp:include page="MainNavs"/><br/> --%>
     <jsp:include page="/MainNavs" /><br/>
         <c:choose>
            <c:when test="${(empty requestScope.authorsData)}">
                <h2>No Authors Exist</h2>
            </c:when>
            <c:otherwise>
                <table border='1'>
                    <tr><th>ID</th><th>Name</th><th>Email</th><th>Ops</th></tr>
                            <c:forEach items="${requestScope.authorsData}" var="author">
                        <tr>
                            <td>${author.authorId}</td>
                            <td>${author.name}</td>
                            <td>${author.email}</td>
                            <td>
                                <c:url var="deleteAuthorURL" value="authors">
                                    <c:param name="aid" value="${author.authorId}"/>
                                </c:url>

                                <a href='<c:out value="${deleteAuthorURL}"/>'>Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
        <br/><br/>

        <sql:setDataSource var="ds" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost:3306/library"
                           user="root" password="root"/>

        <sql:query dataSource="${ds}" var="authorsResult" sql="SELECT * FROM authors"/>


        <table border='1'>
            <tr><th>ID</th><th>Name</th><th>Email</th><th>Ops</th></tr>
                    <c:forEach items="${authorsResult.rowsByIndex}" var="author">
                <tr>
                    <td>${author[0]}</td>
                    <td>${author[1]}</td>
                    <td>${author[2]}</td>
                    <td>
                        <c:url var="deleteAuthorURL2" value="authors">
                            <c:param name="aid" value="${author[0]}"/>
                        </c:url>

                        <a href='<c:out value="${deleteAuthorURL2}"/>'>Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
</body>
</html>