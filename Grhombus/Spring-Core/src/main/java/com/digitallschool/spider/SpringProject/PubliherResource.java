package com.digitallschool.spider.SpringProject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Component;

import com.mysql.cj.protocol.Resultset;


@Component
public class PubliherResource {
	
	@Resource
	private DataSource cd;
	
	public Publisher getPublisher(int i) throws SQLException
	{
		try(Connection con=cd.getConnection();
				PreparedStatement st=con.prepareStatement("select * from publishers where publisher_id=?");
				){
			
			st.setInt(1,i);
			
			ResultSet rs=st.executeQuery();
			
			while(rs.next())
			{
				return (new Publisher(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)));
			}
			
			
		}
		
		
		
		
		return null;
	}
	public void setPublisher(String name,String email,String contact,String address) throws SQLException
	{
		try(Connection con=cd.getConnection();
				
				){
			
			PreparedStatement st=con.prepareStatement("Insert into publishers(publisher_id,name,email,contact,address)"+"values(?,?,?,?,?)");
			
			st.setInt(1, 0);
			st.setString(2,name);
			st.setString(3,email);
			st.setString(4,contact);
			st.setString(5,address);
			
			st.executeUpdate();
			
			System.out.println("Publisher Has been Inserted");
		
		}
		
	
	}

}
