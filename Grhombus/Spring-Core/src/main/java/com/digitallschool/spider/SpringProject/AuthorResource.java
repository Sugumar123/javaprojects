package com.digitallschool.spider.SpringProject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorResource {
	
	@Resource
	private DataSource cd;
	
	public void deleteAuthor(int i) throws SQLException
	{
		try(Connection con=cd.getConnection();
				PreparedStatement ps=con.prepareStatement("delete from authors where author_id=?");){
			
			ps.setInt(1, i);
			ps.executeUpdate();
			
			System.out.println("Author Has been deleted");
		}
		
	
	}
	
	
	

}
