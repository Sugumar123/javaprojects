package com.digitallschool.spider.SpringProject;

public class Author {

	private int author_id;
	private String name;
	private String email;
	private String profile;
	private String blog;
	
	public Author(int id,String name,String email,String profile,String blog)
	{
		this.author_id=id;
		this.name=name;
		this.email=email;
		this.profile=profile;
		this.blog=blog;
		
	}
	public int getAuthor_id() {
		return author_id;
	}
	public void setAuthor_id(int author_id) {
		this.author_id = author_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getBlog() {
		return blog;
	}
	public void setBlog(String blog) {
		this.blog = blog;
	}
	
	
}
