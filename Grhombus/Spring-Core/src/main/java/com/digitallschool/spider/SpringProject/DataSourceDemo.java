package com.digitallschool.spider.SpringProject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DataSourceDemo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		
		ApplicationContext ctx1=new ClassPathXmlApplicationContext("Spring-Core.xml");
		
		DataSource p=ctx1.getBean("db1",DataSource.class);
		
		try(Connection con=p.getConnection();
				Statement sq=con.createStatement();
				ResultSet rs=sq.executeQuery("select* from publishers")){
			
			while(rs.next())
			{
				System.out.println(rs.getInt(1)+" "+rs.getString(2));
			}
		}

	}

}
