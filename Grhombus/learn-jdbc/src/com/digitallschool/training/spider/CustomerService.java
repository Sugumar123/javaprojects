package com.digitallschool.training.spider;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="customer")
public class CustomerService implements Serializable {
	
	
     @Id	
     @Column(name="customer_id")
     int  id;
     @Column(name="name")
     String name;
     @Column (name="mobile")
     String mobile;
     public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	@Column(name="address")
     String add;
    
     
    /* public CustomerService() {
 	setId(i);
 	setName(string);
 	setAdd(string2);
 	setDate1(string4);// TODO Auto-generated constructor stub
 		
 	}*/
     
	
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}

}
