package com.digitallschool.training.spider;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JPAMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("learn-jdbc");
		
		EntityManager em=emf.createEntityManager();
		
		//List<Customers> obj=em.createQuery("SELECT a from Customers a", Customers.class).getResultList();
       
		/*for(Customers e:obj)
        	System.out.println(e.getId()+" "+e.getName()+" "+e.getAdd());*/
		CustomerService c1=new CustomerService();
	
		c1.setId(3);
		c1.setName("Ramesh");
		c1.setMobile("90044");
		c1.setAdd("Chennai");
		
		CustomerService c2=em.find(CustomerService.class,3);
		EntityTransaction tx1=em.getTransaction();
		tx1.begin();
		
		em.remove(c2);
		tx1.commit();
		System.out.println("Row is Removed");
		/*tx.begin();
		em.remove(c1);
		tx.commit();*/
		
		
		
        em.close();
        emf.close();
        
	}

	

}
