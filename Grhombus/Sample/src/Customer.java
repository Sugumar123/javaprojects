import java.time.LocalDate;

public class Customer {
    private String name;
    private int age;
    private String gender;
    private LocalDate dob;
    
    public Customer(String name, int age, String gender, LocalDate dob){
        setName(name);
        setAge(age);
        setGender(gender);
        setDob(dob);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }
}
