package com.digitallschool.SampleWorks;

public class UserService {
	
	public void doWork(ProcessAction t)
	{
		
		System.out.println("Work Started");
		t.process();
		System.out.println("Work Ended");
	}

}
