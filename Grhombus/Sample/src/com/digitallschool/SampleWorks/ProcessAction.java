package com.digitallschool.SampleWorks;


@FunctionalInterface
public interface ProcessAction {
	
   void process();
	
}

