
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution1 {
	
	    public static void main(String[] args) {
	        String[] template = new String[8];
	        
	        try(BufferedReader in = new BufferedReader(new FileReader("E:\\Template.txt")))
	        {
	            for(int i=0; i<8; i+=2){
	                String input = in.readLine();
	                //System.out.println(input);
	                template[i] = input.substring(0, input.indexOf("{"));
	                template[i+1] = input.substring(input.indexOf("{"));
	            }
	        }catch(IOException ioe){
	            ioe.printStackTrace();
	            return;
	        }
	        List<Customer> customerList = new ArrayList<>();
	        customerList.add(new Customer("James", 60, "Male", LocalDate.of(2019-60, 10, 10)));
	        customerList.add(new Customer("Dennis", 30, "Female", LocalDate.of(2019-30, 8, 5)));
	        customerList.add(new Customer("Johnson", 45, "Male", LocalDate.of(2019-45, 6, 22)));
	        
	        for(Customer current: customerList){
	            for(int i=0; i<template.length; i++){
	                String s = template[i];
	                if(s.contains("{{") && current.getAge()>40){
	                    switch(s.substring(2, s.length()-2).trim()){
	                        case "name":
	                            System.out.println(template[i-1] + current.getName());
	                            break;
	                        case "age":
	                            System.out.println(template[i-1] + current.getAge());                            
	                            break;
	                        case "gender":
	                            System.out.println(template[i-1] + current.getGender());                            
	                            break;
	                        case "dob":
	                            System.out.println(template[i-1] + current.getDob());
	                            break;
	                    }
	                }
	            }
	            System.out.println("");
	        }
	    }
	}



