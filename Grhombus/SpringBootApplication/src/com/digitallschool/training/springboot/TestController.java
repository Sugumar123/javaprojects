package com.digitallschool.training.springboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {
	
	@GetMapping
	public String sayHello(@RequestParam(name="name",required=false,defaultValue="world")String name) {
		
		return "Hallo,"+name+"!";
	}

}
