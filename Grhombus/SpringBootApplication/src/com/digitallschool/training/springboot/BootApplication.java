package com.digitallschool.training.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootApplication {
	
	public static void main(String[] args) {
		
		
		SpringApplication app=new SpringApplication(BootApplication.class);
		app.run(args);
				
	}

}
