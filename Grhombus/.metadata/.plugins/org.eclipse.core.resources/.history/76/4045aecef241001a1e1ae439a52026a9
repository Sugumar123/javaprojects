package com.digitallschool.spring_mvc_demo;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authorAPI")
public class AuthorController {
	
	 @Autowired
	 AuthorService service;
	 
	 @GetMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	 public List<Author> getAuthors()
	 {
		 return service.getAllAuthors();
	 }
	 
	 @ModelAttribute
	 public Author setupAuthor(@RequestParam(name="authorId",required=false)String id)
	 {
		 if(Objects.isNull(id)||id.isEmpty())
		 {
			 return null;
		 }
		 int memberId=Integer.parseInt(id);
		 
		 if(memberId== 0)
			  return null;
		 else
			 return service.getAuthorById(memberId); 
	 }
	 
	 @GetMapping(path="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	 public Author getMemberById(@PathVariable("id") int authorId)
	 {
		 
		 return service.getAuthorById(authorId);
	 }
	 @GetMapping(path="/{id}/{property}",produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity getMemberByIdWithProperty(@PathVariable("id")int authorId,@PathVariable(name="property") String property)
	 {
		 Author author=service.getAuthorById(authorId);
		 
		 if(property.equals("name"))
		 {
			 return ResponseEntity.ok(author.getName());
		 }
		 else if(property.equals("email"))
		 {
			 return ResponseEntity.ok(author.getEmail());
		 }
		 return ResponseEntity.ok(author);
	 }
	 @PostMapping
	 public ResponseEntity addAuthor(@Valid @ModelAttribute("author") Author currentAuthor,BindingResult binding)
	 {
		 
		// Author author=new Author(0,"bagya2","bagya@gmail.com","some_profile","unknown");
		 
		// System.out.println(currentAuthor.getAuthorId()+" "+currentAuthor.getEmail());
		 
		 
		 if(!binding.hasErrors() && service.addAuthor(currentAuthor))
		 {
			 return ResponseEntity.ok("Successfully Added");
		 }
		 else
		 {
			 return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
	                    .body("Incomplete or Invalid Data: " + binding.toString());
 
		 }
		 
	 }
	 
	

}
